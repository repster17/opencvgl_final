#include <unistd.h>
#include <vector>
#include <complex>
#include <thread>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include <AL/al.h>
#include <AL/alut.h>
#include "consts.hpp"
#include "fft.hpp"
#include "spectrum.hpp"

// global variables
ALuint buffer, source;
std::vector<double *> spectrumList;
float filesize;
GLuint jacketTexture[1] = {0};
bool isJacketLoaded = false;
double Cb_center = 0, Cr_center = 0;

void FFT_Tempo_Handler(char *filename){
	short *input = (short *)malloc(sizeof(short) * FFTSIZE);
	std::complex<double> *complex = (std::complex<double> *)malloc(sizeof(std::complex<double>) * FFTSIZE);
	std::complex<double> *output = (std::complex<double> *)malloc(sizeof(std::complex<double>) * FFTSIZE);
	std::vector<double> volumeIncreaseList;

	double prevVol = 0.0;

	std::string command;
	command = "sox \"" + std::string(filename) + "\" -r 44100 -c 1 -b 16 -e signed-integer -t raw -";
	FILE *fp = popen(command.c_str(), "r");

	while(true){
		int n = fread(input, sizeof(short), FFTSIZE, fp);
		if(n < FFTSIZE){
			break;
		}
		short_to_complex(input, complex, FFTSIZE);
		FFT(complex, output, FFTSIZE);

		// POINTS_PER_SPECTごとに平均を取る
		double *ffted = (double *)malloc(sizeof(double) * EFFECTIVE_SPECT_NUM);
		for (long i = 1, j = 0; i < FFTSIZE * 100 / 256; i++){
			ffted[j] += norm(output[i]);
			if(i % POINTS_PER_SPECT == POINTS_PER_SPECT - 1){
				ffted[j] /= (j == 0) ? POINTS_PER_SPECT - 1 : POINTS_PER_SPECT;
				if (ffted[j] >= 1.0){
					ffted[j] = log(ffted[j]);
				}else{
					ffted[j] = 0;
				}
				j++;
			}
		}
		
		spectrumList.push_back(ffted);


		// tempo
		{
			double nextVol = 0.0;
			for (int i = 0; i < FFTSIZE; i++){
				nextVol += input[i] * input[i];
			}

			nextVol = sqrt(nextVol / FFTSIZE);

			volumeIncreaseList.push_back((nextVol - prevVol > 0)? nextVol - prevVol : 0);
			nextVol = prevVol;
		}
	}

	// tempo detect
	{
		double R_max = 0;
		int tempo = 0;
		const double s = 44100.0 / FFTSIZE;

		for (int i = 90; i < 180; i++){
			double a = 0, b = 0;
			for (int j = 0; j < volumeIncreaseList.size(); j++){
				const double phase = 2.0 * M_PI * i * j / 60 / s;
				a += volumeIncreaseList[j] * cos(phase);
				b += volumeIncreaseList[j] * sin(phase);
			}
			const double R = sqrt(a * a + b * b);

			if(R > R_max){
				tempo = i;
				R_max = R;
			}
		}

		printf("tempo: %d\n", tempo);
		Cb_center = 0.5 * cos(-2 * M_PI * tempo / 90);
		Cr_center = 0.5 * sin(-2 * M_PI * tempo / 90);
	}

	fclose(fp);
	free(input);
	free(complex);
	free(output);
}


void init(int argc, char* argv[]){
	// Create FFT thread
	std::thread FFT_Thread(FFT_Tempo_Handler, argv[1]);

	// AL
	{
		alGenBuffers(1, &buffer);
		alGenSources(1, &source);
		buffer = alutCreateBufferFromFile(argv[1]);
		alSourcei(source, AL_BUFFER, buffer);
		alSourcei(source, AL_LOOPING, AL_TRUE);

		ALint size, frequency, channels, bits;
		alGetBufferi(buffer, AL_SIZE, &size);
		alGetBufferi(buffer, AL_FREQUENCY, &frequency);
		alGetBufferi(buffer, AL_CHANNELS, &channels);
		alGetBufferi(buffer, AL_BITS, &bits);

		filesize = (float)size / (float)(frequency * channels * (bits / 8));
	}

	// jacket texture
	if(argc == 3){
		glBindTexture(GL_TEXTURE_2D, jacketTexture[0]);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEXTURE_WIDTH, TEXTURE_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		cv::Mat input = cv::imread(argv[2]);
		cv::cvtColor(input, input, CV_BGR2RGB);
		cv::resize(input, input, cv::Size(TEXTURE_WIDTH, TEXTURE_HEIGHT));

		glBindTexture(GL_TEXTURE_2D, jacketTexture[0]);
		glTexSubImage2D(GL_TEXTURE_2D, 0,
						(TEXTURE_WIDTH - input.cols) / 2,
						(TEXTURE_HEIGHT - input.rows) / 2,
						input.cols, input.rows,
						GL_RGB, GL_UNSIGNED_BYTE, input.data);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, jacketTexture[0]);

		isJacketLoaded = true;
	}

	//FFT_Thread.detach();
	FFT_Thread.join();

	// 音源の再生
	alSourcePlay(source);
}

void fin(){
	for (long i = 0; i<spectrumList.size(); i++){
		free(spectrumList[i]);
	}
	spectrumList.clear();
}

float getCurrentTime(){
	float offset;
	alGetSourcef(source, AL_SEC_OFFSET, &offset);
	return offset / filesize;
}

long getOffsetinList(){
	return (long)(spectrumList.size() * getCurrentTime());
}

long getListLength(){
	return spectrumList.size();
}

// draw
void draw_rect(double x1, double y1, double x2, double y2){
	glColor3d(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
	glVertex2d(x1,y1);
	glVertex2d(x2,y1);
	glVertex2d(x2,y2);
	glVertex2d(x1,y2);
	glEnd();
}


void jacket_display(){
	if(isJacketLoaded){
		glPushMatrix();

		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_POLYGON);

		glColor4f(1.0, 1.0, 1.0, 1.0);

		glMultiTexCoord2d(GL_TEXTURE0, 1.0, 0.0); 
		glVertex3d(2.0, 3.0, 0.0);
		glMultiTexCoord2d(GL_TEXTURE0, 0.0, 0.0); 
		glVertex3d(-2.0, 3.0, 0.0);
		glMultiTexCoord2d(GL_TEXTURE0, 0.0, 1.0); 
		glVertex3d(-2.0, -1.0, 0.0);
		glMultiTexCoord2d(GL_TEXTURE0, 1.0, 1.0);
		glVertex3d(2.0, -1.0, 0.0);

		glEnd();
		glDisable(GL_TEXTURE_2D);

		glPopMatrix();
	}
}


void spectrum_display(){
	const long offsetinList = getOffsetinList();

	for (long i = 0; i < EFFECTIVE_SPECT_NUM; i++){
		double size = (double)(spectrumList[offsetinList][i]) * 1.0;

		glPushMatrix();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		const double Cos = cos(-1.0 * i / EFFECTIVE_SPECT_NUM + 0.5);
		const double Sin = sin(-1.0 * i / EFFECTIVE_SPECT_NUM + 0.5);

		const double Y = 0.90;
		const double Cb = Cb_center * Cos - Cr_center * Sin;
		const double Cr = Cb_center * Sin + Cr_center * Cos;

		const double R = Y + 1.402 * Cr;
		const double G = Y - 0.3441 * Cb - 0.7141 * Cr;
		const double B = Y + 1.772 * Cb;

		glRotated(360.0 * i / EFFECTIVE_SPECT_NUM, 0.0, 1.0, 0.0);
		glTranslated(3.0, 3.0, 0.0);

		for (int j = 0; j <= (int)size; j++){
			glColor4f(R, G, B, pow(0.8, (double)(j+1)));

			glutSolidCube(0.25);
			glTranslated(0.0, -0.5, 0.0);
		}

		glDisable(GL_BLEND);

		glPopMatrix();
	}

	glFlush();
}
