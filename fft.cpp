#include <complex>
#include <cmath>

#include <iostream>
#include <thread>
#include <functional>

bool pow2check(long n){
	while(n > 1){
		if(n % 2){
			return false;
		}
		n /= 2;
	}
	return true;
}

void FFT_R(std::complex<double>* x, std::complex<double>* y, long n, const std::complex<double>& w){
	if(n == 1){
		y[0] = x[0];
	} else {
		std::complex<double> W(1.0, 0.0);

		for (long i = 0; i < n / 2; i++){
			y[i]			= (x[i] + x[i + n / 2]);
			y[i + n / 2]	= W * (x[i] - x[i + n / 2]);
			W *= w;
		}

		FFT_R(y,			x,			n / 2, w * w);
		FFT_R(y + n / 2,	x + n / 2,	n / 2, w * w);

		for (long i = 0; i < n / 2; i++){
			y[2 * i]		= x[i];
			y[2 * i + 1]	= x[i + n / 2];
		}
	}
}

void FFT(std::complex<double>* x, std::complex<double>* y, long n){
	if(!pow2check(n)){
		fprintf(stderr, "error: n (%ld) is not a power of two\n", n);
		exit(1);
	}

	double arg = 2.0 * M_PI / n;
	std::complex<double> w(cos(arg), -sin(arg));
	FFT_R(x, y, n, w);
	for (long i = 0; i < n; i++){
		y[i] /= n;
	}
}

void short_to_complex(short * s, std::complex<double>* X, long n) {
	for (long i = 0; i < n; i++){
		X[i] = s[i];
	}
}
