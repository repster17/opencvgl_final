#pragma once

const long FFTSIZE = 2048;
const long SPECT_NUM = 128;
const long EFFECTIVE_SPECT_NUM = SPECT_NUM * 100 / 256;
const long POINTS_PER_SPECT = FFTSIZE / SPECT_NUM;

const int TEXTURE_HEIGHT = 512;
const int TEXTURE_WIDTH = 512;
