#pragma once

#include <complex>

// x: input
// y: output
// n: split size
void FFT(std::complex<double> *x, std::complex<double> *y, long n);

void short_to_complex(short *s, std::complex<double> *X, long n);
