CXX=g++
CXXFLAGS=`pkg-config opencv --cflags` -std=c++11
LDLIBS= -lglut -lGL -lGLU -lalut -lopenal -pthread `pkg-config opencv --libs`
SOURCE := $(shell ls *.cpp)
HEADER := $(shell ls *.hpp)
OBJS := $(SOURCE:.cpp=.o)
TARGET = final

all: clean $(TARGET)

$(TARGET):$(OBJS)
	$(CXX) -o $@ $(OBJS) $(LDLIBS)

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $^

clean:
	rm -f *.o
