#include <iostream>
#include <cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include <AL/al.h>
#include <AL/alut.h>
#include "spectrum.hpp"

#define WINDOW_X (500)
#define WINDOW_Y (500)
#define WINDOW_NAME "final"

// global variables
bool g_isLeftButtonOn = false;
bool g_isRightButtonOn = false;
double g_angle1 = 0.0;
double g_angle2 = 0.5;
double g_distance = 20.0;


// funcs
void init_GL(int, char **);
void init_AL(int, char **);
void fin_AL();
void set_callback_functions();

// callbacks
void glut_keyboard(unsigned char key, int x, int y);
void glut_mouse(int button, int state, int x, int y);
void glut_motion(int x, int y);
void glut_idle();
void glut_display();


int main(int argc, char* argv[]){
	if(argc == 1){
		std::cout << "[Usage] ./test [wav file] (jacket file)" << std::endl;
		return 0;
	}

	init_GL(argc, argv);
	init_AL(argc, argv);
	init(argc, argv);
	set_callback_functions();

	glutMainLoop();

	fin();
	fin_AL();
	return 0;
}

void init_GL(int argc, char *argv[]){
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(WINDOW_X, WINDOW_Y);
	glutCreateWindow(WINDOW_NAME);

	glClearColor(0.1, 0.2, 0.3, 0.0);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
}

void init_AL(int argc, char *argv[]){
	alutInit(&argc, argv);
}

void fin_AL(){
	alutExit();
}

void set_callback_functions(){
	glutDisplayFunc(glut_display);
	glutKeyboardFunc(glut_keyboard);
	glutMouseFunc(glut_mouse);
	glutMotionFunc(glut_motion);
	glutPassiveMotionFunc(glut_motion);
	glutIdleFunc(glut_idle);
}


// callbacks
void glut_keyboard(unsigned char key, int x, int y){
	switch(key){
	case 'q':
	case 'Q':
	case '\033':
		//exit(0);
		glutLeaveMainLoop();	// glutのMainLoopを抜けることでalutExitを実行できるようにする
		break;

	case 'd':
		g_angle1 += 0.05;
		break;
	case 'a':
		g_angle1 -= 0.05;
		break;
	case 'w':
		g_angle2 += 0.05;
		break;
	case 's':
		g_angle2 -= 0.05;
		break;
	};

	glutPostRedisplay();
}

void glut_mouse(int button, int state, int x, int y){
	if(button == GLUT_LEFT_BUTTON){
		if(state == GLUT_UP){
			g_isLeftButtonOn = false;
		}else if(state == GLUT_DOWN){
			g_isLeftButtonOn = true;
		}
	}else if(button == GLUT_RIGHT_BUTTON){
		if(state == GLUT_UP){
			g_isRightButtonOn = false;
		}else if(state == GLUT_DOWN){
			g_isRightButtonOn = true;
		}
	}
}

void glut_motion(int x, int y){
	static int px = -1, py = -1;

	if(g_isLeftButtonOn){
		if(px >= 0 && py >= 0){
			g_angle1 += (double)-(x - px)/100;
			g_angle2 += (double)(y - py)/20;
		}
		px = x;
		py = y;
	}else if(g_isRightButtonOn){
		if(px >= 0 && py >= 0){
			g_distance += (double)(y - py)/20;
		}
		px = x;
		py = y;
	}else{
		px = -1;
		py = -1;
	}
	glutPostRedisplay();
}

void glut_idle(){
	static long prev_offsetinList = -1;

	const long offsetinList = getOffsetinList();

	if(prev_offsetinList != offsetinList && offsetinList < getListLength()){
		prev_offsetinList = offsetinList;
		glutPostRedisplay();

		g_angle1 += 360.0 / 100000;
	}
}


void glut_display(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30.0, 1.0, 0.1, 100);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (cos(g_angle2)>0){
	gluLookAt(g_distance * cos(g_angle2) * sin(g_angle1), 
			g_distance * sin(g_angle2), 
			g_distance * cos(g_angle2) * cos(g_angle1), 
			0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	}else{
	gluLookAt(g_distance * cos(g_angle2) * sin(g_angle1),
			g_distance * sin(g_angle2),
			g_distance * cos(g_angle2) * cos(g_angle1),
			0.0, 0.0, 0.0, 0.0, -1.0, 0.0);
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	glPushMatrix();
	jacket_display();
	glPopMatrix();

	glPushMatrix();
	spectrum_display();
	glPopMatrix();

	glFlush();

	glDisable(GL_DEPTH_TEST);

	glutSwapBuffers();
}

